
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class CodeEditorWorkspaceGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(appName, workspaceName) {
    if(undefined !== appName) {
      this.asynchReadFileResponse(ActorPathGenerated.getWorkspaceFile(appName, workspaceName));
    }
    else {
      this.asynchReadFile(ActorPathGenerated.getWorkspaceRecentFiles(), (err, data) => {
        if(err) {
          return this.expectAsynchResponseError(`Could not get recent project files '${ActorPathGenerated.getWorkspaceRecentFiles()}'.`, err);
        }
        else {
          const lastWorkspace = data.recentWorkspaces[0];
          this.asynchReadFileResponse(ActorPathGenerated.getWorkspaceFile(lastWorkspace.appName, lastWorkspace.workspaceName));
        }
      });
    }
  }
}

module.exports = CodeEditorWorkspaceGet;
