
'use strict';

const ActorPathProject = require('z-abs-corelayer-server/path/actor-path-project');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class CodeEditorFolderNew extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(file) {
    this.asynchMkdirResponse(ActorPathProject.getFile(file));
  }
}


module.exports = CodeEditorFolderNew;
