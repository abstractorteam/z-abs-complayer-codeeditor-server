
'use strict';

const ActorPathProject = require('z-abs-corelayer-server/path/actor-path-project');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');
const Os = require('os');


class CodeEditorFileNew extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(path, name, type, templateName) {
    this._create(path, name, type, templateName);
  }
  
  _create(path, name, type, templateName) {
    const fileName = `${name}.${type}`;
    const text = CodeEditorFileNew.templates.get(templateName)({
      name: `${name[0].toUpperCase()}${name.slice(1)}`,
      ups: this._getUps(path),
      nameUpperCase: this._formatName(name),
    });
    const file = ActorPathProject.getFile(`${path}/${fileName}`);
    this.asynchWriteTextFileResponse(file, text, fileName);
  }
  
  _getUps(path) {
    return '../'.repeat(path.split('/').length - 1);
  }
  
  _formatNamePart(namePart) {
    if(0 === namePart.length) {
      return '';
    }
    else if(1 === namePart.length) {
      return `${namePart[0].toUpperCase()}`;
    }
    else {
      return `${namePart[0].toUpperCase()}${namePart.slice(1)}`;
    }
  }
  
  _formatName(name) {
    const nameParts = name.split('-');
    const newNameParts = nameParts.map((namePart) => {
      return this._formatNamePart(namePart);
    });
    return newNameParts.join('');
  }
  
  static _template(strings, ...keys) {
    return ((...values) => {
      const dict = values[values.length - 1] || {};
      const result = [strings[0].replace(new RegExp('\n', 'g'), Os.EOL)];
      keys.forEach((key, i) => {
        const value = Number.isInteger(key) ? values[key] : dict[key];
        result.push(value, strings[i + 1].replace(new RegExp('\n', 'g'), Os.EOL));
      });
      return result.join('');
    });
  }
}

CodeEditorFileNew.templates = new Map([
  [
    'Class [class js file]',
    CodeEditorFileNew._template`
'use strict';


class ${'nameUpperCase'} {
  constructor() {
    
  }
}

module.exports = ${'nameUpperCase'};
`
  ],
  [
    'None [empty js file]',
    CodeEditorFileNew._template``
  ],
  [
    'React Base',
    CodeEditorFileNew._template``
  ],
  [
    'React Store',
    CodeEditorFileNew._template``
  ],
  [
    'React Realtime',
    CodeEditorFileNew._template``
  ],
  [
    'None [empty jsx file]',
    CodeEditorFileNew._template``
  ]
]);


module.exports = CodeEditorFileNew;
