
'use strict';

const ActorPathProject = require('z-abs-corelayer-server/path/actor-path-project');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class CodeEditorFileGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(file) {
    this.asynchReadTextFileResponse(ActorPathProject.getFile(file));
  }
}

module.exports = CodeEditorFileGet;
