
'use strict';

const ActorPathProject = require('z-abs-corelayer-server/path/actor-path-project');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class CodeEditorFileUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(file, data) {
    this.asynchWriteTextFileResponse(ActorPathProject.getFile(file), data);
  }
}

module.exports = CodeEditorFileUpdate;
