
'use strict';

const ActorPathProject = require('z-abs-corelayer-server/path/actor-path-project');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class CodeEditorFolderDelete extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.DELETE);
  }
  
  onRequest(file) {
    this.asynchRmdirResponse(ActorPathProject.getFile(file), true);
  }
}


module.exports = CodeEditorFolderDelete;
