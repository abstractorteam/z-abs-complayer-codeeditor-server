
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class CodeEditorWorkspaceNew extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(appName, workspaceName) {
    const workspace = {
      settings: {},
      projects: []
    };
    this.asynchWriteFileResponse(ActorPathGenerated.getWorkspaceFile(appName, workspaceName), workspace, workspace);
  }
}

module.exports = CodeEditorWorkspaceNew;
