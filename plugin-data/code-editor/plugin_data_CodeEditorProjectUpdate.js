
'use strict';

const ActorPathProject = require('z-abs-corelayer-server/path/actor-path-project');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class CodeEditorProjectUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(content) {
    this.asynchWriteFileResponse(ActorPathProject.getCodeProjectFile(), content, false);
  }
}

module.exports = CodeEditorProjectUpdate;
