
'use strict';

const ActorPathProject = require('z-abs-corelayer-server/path/actor-path-project');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class CodeEditorFileRename extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(oldPath, newPath) {
    this.asynchRenameResponse(ActorPathProject.getFile(oldPath), ActorPathProject.getFile(newPath));
  }
}


module.exports = CodeEditorFileRename;
