
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/path/actor-path-generated');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class CodeEditorWorkspaceOpen extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(file) {
    if(undefined !== file) {
      this.asynchReadFileResponse(ActorPathGenerated.getWorkspaceRecentFiles());
    }
    else {
      this.asynchReadFileResponse(ActorPathGenerated.getWorkspaceRecentFiles());
    }
  }
}

module.exports = CodeEditorWorkspaceOpen;
