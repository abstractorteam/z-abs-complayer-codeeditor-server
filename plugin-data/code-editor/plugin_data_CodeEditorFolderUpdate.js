
'use strict';

const ActorPathProject = require('z-abs-corelayer-server/path/actor-path-project');
const PluginBaseMulti = require('z-abs-corelayer-server/plugin-base-multi');


class CodeEditorFolderUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(oldPath, newPath) {
    this.asynchRenameResponse(ActorPathProject.getFile(oldPath), ActorPathProject.getFile(newPath));
  }
}


module.exports = CodeEditorFolderUpdate;
